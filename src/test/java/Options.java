import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.testng.Assert.assertTrue;

/**
 * Created by User on 09.07.2017.
 */
public class Options extends CalculatorMain {
    public enum operatorValues {
        ADDITION, DIVISION, MODULO, MULTIPLICATION, SUBTRACTION;
    }

    // option location and value from UI
    static WebElement optionLocatorModulo=driver.findElement(By.xpath("/html/body/div/div/form/select/option[3]"));
    static String valueOfOptionModulo=optionLocatorModulo.getAttribute("value");

    static WebElement optionLocatorAddition=driver.findElement(By.xpath("/html/body/div/div/form/select/option[1]"));
    static String valueOfOptionAddition=optionLocatorAddition.getAttribute("value");

    static WebElement optionLocatorDivision=driver.findElement(By.xpath("/html/body/div/div/form/select/option[2]"));
    static String valueOfOptionDivision=optionLocatorDivision.getAttribute("value");

    static WebElement optionLocatorMultiplication=driver.findElement(By.xpath("/html/body/div/div/form/select/option[4]"));
    static String valueOfOptionMultiplication=optionLocatorMultiplication.getAttribute("value");

    static WebElement optionLocatorSubtraction=driver.findElement(By.xpath("/html/body/div/div/form/select/option[5]"));
    static String valueOfOptionSubtraction=optionLocatorSubtraction.getAttribute("value");


    public static String getOptionValue(String optionValueWebelement,String enumValue){
        optionValueWebelement= String.valueOf(operatorValues.valueOf(enumValue));
        System.out.println(optionValueWebelement);
        return optionValueWebelement;
    }

    public void getSelectionState(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        assertTrue(wait.until(ExpectedConditions.
                elementSelectionStateToBe((element), true)));
    }

    public void waitForTextInResult(String text){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.textToBePresentInElement(driver.findElement(By.xpath("//h2[@class]")),text));
    }

}
