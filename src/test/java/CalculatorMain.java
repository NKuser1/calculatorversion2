import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;

/**
 * Created by User on 09.07.2017.
 */
public class CalculatorMain {
    public static WebDriver driver;

    @BeforeTest
    public void setup() {

        final File file = new File(PropertyLoader.loadProperty("path.webDriver"));
        System.setProperty(PropertyLoader.loadProperty("webDriver"), file.getAbsolutePath());
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }


    @Test(priority = 1)
    public void openPage() {
        driver.navigate().to("http://juliemr.github.io/protractor-demo/");
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver, 12);
        wait.until(ExpectedConditions.urlToBe("http://juliemr.github.io/protractor-demo/"));
    }

    @Test(priority = 2)
    public void additionUpdatedTest() {
        WebElement firstValue = driver.findElement(By.xpath("//input[@ng-model='first']"));
        firstValue.sendKeys("1");
        System.out.println(firstValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(firstValue.getAttribute("value"), "1");

        //input second value
        WebElement secondValue = driver.findElement(By.xpath("//input[@ng-model='second']"));
        secondValue.sendKeys("1");
        System.out.println(secondValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(secondValue.getAttribute("value"), "1");

        //selectOption
        WebElement operator = driver.findElement(By.xpath("//select[@ng-model='operator']"));
        Select operatorSelect = new Select(operator);
        operatorSelect.selectByValue(Options.getOptionValue(Options.valueOfOptionModulo,"ADDITION"));

        // clickButton
        WebElement buttonGo = driver.findElement(By.xpath("//button[@ng-click='doAddition()']"));
        buttonGo.click();
        Options resultWaitMethod = new Options();
        resultWaitMethod.waitForTextInResult("2");
        // result value
        WebElement result = driver.findElement(By.xpath("//h2[@class]"));
        //assert result
        assertEquals(result.getText(), "2");
    }

    @Test(priority = 3)
    public void divisionUpdatedTest() {
        WebElement firstValue = driver.findElement(By.xpath("//input[@ng-model='first']"));
        firstValue.sendKeys("4");
        System.out.println(firstValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(firstValue.getAttribute("value"), "4");

        //input second value
        WebElement secondValue = driver.findElement(By.xpath("//input[@ng-model='second']"));
        secondValue.sendKeys("4");
        System.out.println(secondValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(secondValue.getAttribute("value"), "4");

        //selectOption
        WebElement operator = driver.findElement(By.xpath("//select[@ng-model='operator']"));
        Select operatorSelect = new Select(operator);
        operatorSelect.selectByValue(Options.getOptionValue(Options.valueOfOptionModulo,"DIVISION"));

        // clickButton
        WebElement buttonGo = driver.findElement(By.xpath("//button[@ng-click='doAddition()']"));
        buttonGo.click();
        Options resultWaitMethod = new Options();
        resultWaitMethod.waitForTextInResult("1");
        // result value
        WebElement result = driver.findElement(By.xpath("//h2[@class]"));
        //assert result
        assertEquals(result.getText(), "1");
    }
    @Test(priority = 4)
    public void moduloUpdatedTest() {
        WebElement firstValue = driver.findElement(By.xpath("//input[@ng-model='first']"));
        firstValue.sendKeys("15");
        System.out.println(firstValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(firstValue.getAttribute("value"), "15");

        //input second value
        WebElement secondValue = driver.findElement(By.xpath("//input[@ng-model='second']"));
        secondValue.sendKeys("7");
        System.out.println(secondValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(secondValue.getAttribute("value"), "7");

        //selectOption
        WebElement operator = driver.findElement(By.xpath("//select[@ng-model='operator']"));
        Select operatorSelect = new Select(operator);
        operatorSelect.selectByValue(Options.getOptionValue(Options.valueOfOptionModulo,"MODULO"));

        // clickButton
        WebElement buttonGo = driver.findElement(By.xpath("//button[@ng-click='doAddition()']"));
        buttonGo.click();
        Options resultWaitMethod = new Options();
        resultWaitMethod.waitForTextInResult("1");
        // result value
        WebElement result = driver.findElement(By.xpath("//h2[@class]"));
        //assert result
        assertEquals(result.getText(), "1");
    }

    @Test(priority = 5)
    public void multiplicationUpdatedTest() {
        WebElement firstValue = driver.findElement(By.xpath("//input[@ng-model='first']"));
        firstValue.sendKeys("9");
        System.out.println(firstValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(firstValue.getAttribute("value"), "9");

        //input second value
        WebElement secondValue = driver.findElement(By.xpath("//input[@ng-model='second']"));
        secondValue.sendKeys("3");
        System.out.println(secondValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(secondValue.getAttribute("value"), "3");

        //selectOption
        WebElement operator = driver.findElement(By.xpath("//select[@ng-model='operator']"));
        Select operatorSelect = new Select(operator);
        operatorSelect.selectByValue(Options.getOptionValue(Options.valueOfOptionModulo,"MULTIPLICATION"));

        // clickButton
        WebElement buttonGo = driver.findElement(By.xpath("//button[@ng-click='doAddition()']"));
        buttonGo.click();
        Options resultWaitMethod = new Options();
        resultWaitMethod.waitForTextInResult("27");
        // result value
        WebElement result = driver.findElement(By.xpath("//h2[@class]"));
        //assert result
        assertEquals(result.getText(), "27");
    }
    @Test(priority = 6)
    public void subtractionUpdatedTest() {
        WebElement firstValue = driver.findElement(By.xpath("//input[@ng-model='first']"));
        firstValue.sendKeys("11");
        System.out.println(firstValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(firstValue.getAttribute("value"), "11");

        //input second value
        WebElement secondValue = driver.findElement(By.xpath("//input[@ng-model='second']"));
        secondValue.sendKeys("1");
        System.out.println(secondValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(secondValue.getAttribute("value"), "1");

        //selectOption
        WebElement operator = driver.findElement(By.xpath("//select[@ng-model='operator']"));
        Select operatorSelect = new Select(operator);
        operatorSelect.selectByValue(Options.getOptionValue(Options.valueOfOptionModulo,"SUBTRACTION"));

        // clickButton
        WebElement buttonGo = driver.findElement(By.xpath("//button[@ng-click='doAddition()']"));
        buttonGo.click();
        Options resultWaitMethod = new Options();
        resultWaitMethod.waitForTextInResult("10");
        // result value
        WebElement result = driver.findElement(By.xpath("//h2[@class]"));
        //assert result
        assertEquals(result.getText(), "10");
    }
}
